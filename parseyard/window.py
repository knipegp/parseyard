"""Parse Chipyard trace and produce branch window stats"""
# /usr/bin/env python3
import argparse
from dataclasses import dataclass
from pathlib import Path

from matplotlib.axes import Axes
from matplotlib import pyplot
import numpy
import pandas
import seaborn


@dataclass
class Stats:
    """Statistics for branch windows in a trace"""

    mean: float
    std: float
    maximum: float
    minimum: float
    per_90: float
    hist: numpy.ndarray

    def __str__(self) -> str:
        return (
            f"mean: {self.mean}, std: {self.std}, max: {self.maximum}, min:"
            f"{self.minimum}, 90th: {self.per_90}"
        )


def from_csv():
    """Save the raw CSV as a pandas binary"""
    parser = argparse.ArgumentParser()
    parser.add_argument("in_path", help="path to CSV file")
    parser.add_argument("out_path", help="path to new numpy binary file")
    args = parser.parse_args()
    arr = numpy.loadtxt(Path(args.in_path).expanduser(), delimiter=",")
    # numpy doesn't seem to like mixing bases so I'll have to save each array individually
    mis_cnt = numpy.array(arr[:, 0], dtype=int)
    mis_pc = numpy.array(arr[:, 1], dtype=int)
    mis_mask = numpy.array(arr[:, 2], dtype=int)
    ev_pc = numpy.array(arr[:, 3], dtype=int)
    ev_mask = numpy.array(arr[:, 4], dtype=int)
    numpy.savez_compressed(
        Path(args.out_path).expanduser(),
        mispred_cnt=mis_cnt,
        mispred_pc=mis_pc,
        mispred_mask=mis_mask,
        evict_pc=ev_pc,
        evict_mask=ev_mask,
    )


def trace_to_pandas(path: Path) -> pandas.DataFrame:
    """Convert the saved numpy binary to pandas dataframe"""
    with numpy.load(path.expanduser()) as arrs:
        mat = numpy.hstack(
            (
                numpy.swapaxes([arrs["mispred_cnt"]], 0, 1),
                numpy.swapaxes([arrs["mispred_pc"]], 0, 1),
                numpy.swapaxes([arrs["mispred_mask"]], 0, 1),
                numpy.swapaxes([arrs["evict_pc"]], 0, 1),
                numpy.swapaxes([arrs["evict_mask"]], 0, 1),
            )
        )
    return pandas.DataFrame(
        mat,
        columns=["mispred_cnt", "mispred_pc", "mispred_mask", "evict_pc", "evict_mask"],
    )


def get_window_stats(frame: pandas.DataFrame) -> Stats:
    """Get statistics for all branch windows in a trace"""
    id_max: int = frame.mispred_cnt.values.max()
    counts = numpy.array([0] * (id_max + 1), dtype=int)
    for wid in range(0, id_max + 1):
        counts[wid] = numpy.where(frame.mispred_cnt.values == wid)[0].shape[0]
    return Stats(
        counts.mean(),  # type: ignore
        counts.std(),  # type: ignore
        counts.max(),  # type: ignore
        counts.min(),  # type: ignore
        numpy.percentile(counts, 90),  # type: ignore
        counts,
    )


def plot_hist(stats: Stats) -> Axes:
    """Plot a histogram of all branch window lengths"""
    plot: Axes = seaborn.histplot(stats.hist, binwidth=1)
    plot.set_xlabel("Transient instruction count")
    return plot


def save_hist():
    parser = argparse.ArgumentParser()
    parser.add_argument("np_path", help="path to numpy binary containing branch trace")
    parser.add_argument("fig_path", help="path to save the histogram")
    args = parser.parse_args()
    frame = trace_to_pandas(Path(args.np_path))
    stats = get_window_stats(frame)
    plot_hist(stats)
    pyplot.savefig(Path(args.fig_path).expanduser())
    print(stats)
