"""Enumerate SonicBOOM consts"""
#!/usr/bin/env python3

from enum import Enum


class FuncUnitCode(Enum):
    """Functional unit codes from the SonicBOOM HDL"""

    ALU = 1
    JMP = 2
    MEM = 4
    MUL = 8
    DIV = 16
    CSR = 32
    FPU = 64
    FDV = 128
    I2F = 256
    F2I = 512
