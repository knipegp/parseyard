"""Support CPI measurements for Verilator simulations"""
#!/usr/bin/env python3

import argparse
from pathlib import Path
from typing import Dict

import numpy
import pandas

from parseyard.boom import FuncUnitCode
from parseyard.window import Stats


def from_csv():
    """Save the raw CSV as a pandas binary"""
    parser = argparse.ArgumentParser()
    parser.add_argument("in_path", help="path to CSV file")
    parser.add_argument("out_path", help="path to new numpy binary file")
    args = parser.parse_args()
    arr = numpy.loadtxt(Path(args.in_path).expanduser(), delimiter=",")
    # numpy doesn't seem to like mixing bases so I'll have to save each array individually
    dec_cycle = numpy.array(arr[:, 0], dtype=int)
    com_cycle = numpy.array(arr[:, 1], dtype=int)
    insn_pc = numpy.array(arr[:, 2], dtype=int)
    fu_id = numpy.array(arr[:, 3], dtype=int)
    numpy.savez_compressed(
        Path(args.out_path).expanduser(),
        dec_cycle=dec_cycle,
        com_cycle=com_cycle,
        insn_pc=insn_pc,
        fu_id=fu_id,
    )


def trace_to_pandas(path: Path) -> pandas.DataFrame:
    """Convert the saved numpy binary to pandas dataframe"""
    with numpy.load(path.expanduser()) as arrs:
        mat = numpy.hstack(
            (
                numpy.swapaxes([arrs["dec_cycle"]], 0, 1),
                numpy.swapaxes([arrs["com_cycle"]], 0, 1),
                numpy.swapaxes([arrs["insn_pc"]], 0, 1),
                numpy.swapaxes([arrs["fu_id"]], 0, 1),
            )
        )
    return pandas.DataFrame(
        mat,
        columns=["dec_cycle", "com_cycle", "insn_pc", "fu_id"],
    )


def get_window_stats(frame: pandas.DataFrame) -> Dict[FuncUnitCode, Stats]:
    """Get statistics for CPI"""
    fu_codes = numpy.unique(frame.fu_id.values)
    stats: Dict[FuncUnitCode, Stats] = dict()
    for fu_code in fu_codes:
        fu_commits = frame.loc[frame["fu_id"] == fu_code]
        cpis = fu_commits.com_cycle.values - fu_commits.dec_cycle.values
        stats[FuncUnitCode(fu_code)] = Stats(
            cpis.mean(),  # type: ignore
            cpis.std(),  # type: ignore
            cpis.max(),  # type: ignore
            cpis.min(),  # type: ignore
            numpy.percentile(cpis, 90),  # type: ignore
            cpis,
        )
    return stats


def save_hist():
    parser = argparse.ArgumentParser()
    parser.add_argument("np_path", help="path to numpy binary containing branch trace")
    args = parser.parse_args()
    frame = trace_to_pandas(Path(args.np_path))
    stats = get_window_stats(frame)
    for fu_id in stats:
        print(f"Functional Unit {fu_id} Stats: {stats[fu_id]}")
